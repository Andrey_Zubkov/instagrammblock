<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{jtinstagram}prestashop>jtinstagram_2d917ae57afc160267033846b9b821cc'] = 'Слайдер фотографий из профиля Instagram';
$_MODULE['<{jtinstagram}prestashop>jtinstagram_a1497383db05f5bdf0ce539b639ac6c4'] = 'Отображает блок фотографий профиля из Instagram на главной страницы сайта.';
$_MODULE['<{jtinstagram}prestashop>jtinstagram_f4f70727dc34561dfde1a3c529b6205c'] = 'Настройки';
$_MODULE['<{jtinstagram}prestashop>jtinstagram_4a602b81f5e6e75acfe124e98fbe21a4'] = 'Введите действующий профиль Instagram.';
$_MODULE['<{jtinstagram}prestashop>jtinstagram_2ac2edd9496ddbb9efcc7aece4d7b2de'] = 'Instagram профиль';
$_MODULE['<{jtinstagram}prestashop>jtinstagram_9bcea81a68629ab358eab049ce3172d3'] = 'Количество фотографий, которые будут показаны на домашней страницы.';
$_MODULE['<{jtinstagram}prestashop>jtinstagram_e93f994f01c537c4e2f7d8528c3eb5e9'] = 'Количество.';
$_MODULE['<{jtinstagram}prestashop>jtinstagram_c9cc8cce247e49bae79f15173ce97354'] = 'Сохранить';
$_MODULE['<{jtinstagram}prestashop>instagram_e91a2724ab809b16483594973816a191'] = 'Наш инстаграм';
