<?php
/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class JtInstagram extends Module
{
    protected $config_form = false;

    protected $def_prof = 'jaguar_team';

    protected $def_count_photos = 20;

    public $ENDPOINTS_ACCOUNT_MEDIA = 'https://www.instagram.com/{username}/media?max_id={max_id}';

    public function __construct()
    {
        $this->name = 'jtinstagram';
        $this->tab = 'front_office_features';
        $this->version = '1.0';
        $this->author = 'Jaguar-Team';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Slider photos of Instagram');
        $this->description = $this->l('Display photos of instagram profile');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayHome');
    }

    public function uninstall()
    {
        Configuration::deleteByName('INSTAGRAM_PROFILE');
        Configuration::deleteByName('INSTAGRAM_COUNT_PHOTOS');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitSave')) == true) {
            $this->postProcess();
        }

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitSave';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'col'           => 6,
                        'type'          => 'text',
                        'prefix'        => '<i class="icon icon-user"></i>',
                        'desc'          => $this->l('Enter a valid Instagram profile.'),
                        'name'          => 'INSTAGRAM_PROFILE',
                        'label'         => $this->l('Instagram profile'),
                        'placeholder'   => 'jaguar_team',
                    ),
                    array(
                        'col'           => 6,
                        'type'          => 'text',
                        // 'prefix'        => '<i class="icon icon-user"></i>',
                        'desc'          => $this->l('Count photos of Instagram which showing at home page.'),
                        'name'          => 'INSTAGRAM_COUNT_PHOTOS',
                        'label'         => $this->l('Count'),
                        'placeholder'   => '20',
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'INSTAGRAM_PROFILE'         => Configuration::get('INSTAGRAM_PROFILE'),
            'INSTAGRAM_COUNT_PHOTOS'    => Configuration::get('INSTAGRAM_COUNT_PHOTOS'),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addJS($this->_path.'/views/slick/slick.min.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
        $this->context->controller->addCss($this->_path.'/views/slick/slick.css');
        
    }

    public function hookDisplayHome()
    {
        $this->context->smarty->assign(
            array(
                'path'  => $this->_path,
                'data'  => $this->getPhotos()
            )
        );

        return $this->display(__FILE__, 'instagram.tpl');
    }


    public function getPhotos($max_id = '') {

        $count = 1;
        $data = 1;

        $count_photo = floor((Configuration::get('INSTAGRAM_COUNT_PHOTOS') ? Configuration::get('INSTAGRAM_COUNT_PHOTOS') : $this->def_count_photos) / 20);

        $more_photos = Configuration::get('INSTAGRAM_COUNT_PHOTOS') % 20;

        $count_photo = (int)($more_photos ? $count_photo + 1 : $count_photo);

        for ($i = 0; $i < $count_photo; $i++) {

            $result = $this->getJsonMediaItems($max_id);
            foreach ($result->items as $key => $value) {

                if ($i == ($count_photo - 1) && $more_photos && $key == $more_photos)
                    break;

                if ($count == 1) {
                    $img[$data][$count] = $value->images->low_resolution->url;
                    $count = 2;
                    continue;
                }

                if ($count == 2) {
                    $img[$data][$count] = $value->images->low_resolution->url;
                    $count = 1;
                    $data++;
                    continue;
                }
            }

            if (!$result->more_available)
                break;

            $max_id = $result->items[count($result->items) - 1]->id;
        }

        return $img;
    }

    public function getJsonMediaItems($max_id = '', $json = true) {
        $user = (Configuration::get('INSTAGRAM_PROFILE') ? Configuration::get('INSTAGRAM_PROFILE') : $this->def_prof);

        $url = str_replace('{username}', $user , $this->ENDPOINTS_ACCOUNT_MEDIA);
        $url = str_replace('{max_id}', $max_id , $url);

        return ($json ? json_decode(file_get_contents($url)) : file_get_contents($url));
    }
}
