<div class="sf-contener instagram-block">

    <div class="title">
        <h3>{l s='Our Instagram' mod='jtinstagram'}</h3>
    </div>

    <div class="instagram-block-slider">

        {foreach from=$data item=photo}
        <div class="instagram-container">
            {foreach from=$photo item=photo_data}
                <div class="img"><img src="{$photo_data}" alt="example" /></div>
            {/foreach}
        </div>
        {/foreach}

    </div>
</div>